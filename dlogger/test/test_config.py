# PyTest tests for the configuration class
#
# Indrek Synter 2019
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import os

from dlogger.config import Config


class TestClass:
    PATH_TEMP_CONFIG = "temp_config.cfg"

    @classmethod
    def teardown_class(cls):
        """Remove any temporary files."""
        os.remove(cls.PATH_TEMP_CONFIG)

    def test_clock(self):
        """Test if the clock device can be configured."""
        with open(self.PATH_TEMP_CONFIG, "wt") as fo:
            fo.write("clock = add_device('CLOCK', 'computer time')\n")
            fo.write("clock.open()\n")
            fo.write("clock.add_channel('Date', 'DATE')\n")
            fo.write("clock.add_channel('Epoch', 'EPOCH')\n")
            fo.write("clock.add_channel('Time', 'SECONDS')\n")

        cfg = Config(self.PATH_TEMP_CONFIG)
        devs = cfg.get_devices()

        assert len(devs) == 1
        assert devs[0].class_name == 'CLOCK'
        assert devs[0].nickname == 'computer time'
        assert devs[0].num_channels() == 3
        assert devs[0].list_channels() == ['Date', 'Epoch', 'Time']

    def test_mcc_usb_temp(self):
        """Test if a simulated MCC USB TEMP can be configured."""
        with open(self.PATH_TEMP_CONFIG, "wt") as fo:
            fo.write("temp = add_device('MCC_USB_TEMP', simulate=True)\n")
            fo.write("temp.open()\n")
            # A resistance measurement channel.
            fo.write("temp.add_channel('R0', 'R0', 'x', 'Ohm')\n")
            # A thermistor measurement channel.
            fo.write("temp.add_channel('T1', 'R1', '-4.02885E-04*x**2 + 6.91783E-01*x - 1.36453E+02', '*C')\n")
            # A PT100 measurement channel.
            fo.write("temp.add_ptx_channel('T2', 'R2',"
                     " 100.1863, 3.8766e-3, -4.3986e-7, -2.0332e-10,"
                     " 0.001, 'kelvin_to_celsius(x)', '*C')\n")

        expected_channels = ['R0 / Ohm', 'T1 / *C', 'T2 / *C']

        cfg = Config(self.PATH_TEMP_CONFIG)
        devs = cfg.get_devices()

        assert len(devs) == 1
        assert devs[0].class_name == 'MCC_USB_TEMP'
        assert devs[0].num_channels() == len(expected_channels)
        assert devs[0].list_channels() == expected_channels
