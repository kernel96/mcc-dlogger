# PyTest tests for the channel classes
#
# Indrek Synter 2019
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

from dlogger.channel.generic_channel import Channel
from dlogger.channel.adrange_channel import ADRangeChannel
from dlogger.channel.ptx_channel import PTXChannel


class TestClass:
    def test_generic_source(self):
        """Test if the generic channel source function works."""
        assert Channel("Alice", "R04").get_source() == ('R', 4)
        assert Channel("Bob", "CJC0").get_source() == ('CJC', 0)
        assert Channel("Charlie", "NoIndex").get_source() == ('NoIndex', 0)

    def test_generic_apply(self):
        """Test if a simple calibration formula works."""
        assert Channel("Alice", "R0", "x", "Ohm").apply_calib(230.15) == ('Alice', 230.15, 'Ohm')
        assert Channel("Bob", "R1", "1 + 2e-2*x**2", "Whatevz").apply_calib(11.2) == ('Bob', 3.5088, 'Whatevz')
        assert Channel("Charlie", "R2", "math.sqrt(x)", "Whatevz").apply_calib(1.21) == ('Charlie', 1.1, 'Whatevz')

    def test_ptx_channel_default(self):
        """Test if PT100 / PT1000 measurement channel works."""
        ptx = PTXChannel("Alice", "R3", 100.1863)
        nickname, val, unit = ptx.apply_calib(107.79)

        assert ptx.get_source() == ('R', 3)

        assert nickname == 'Alice'
        assert unit == 'K'
        # 107.79 Ohm should be around 293 K (20 *C)
        assert round(val + 0.5) == 293

    def test_ptx_channel_celsius(self):
        """Test if PT100 / PT1000 measurement channel Kelvin to Celsius conversion works."""
        ptx = PTXChannel("Alice", "R3", 100.1863, formula="kelvin_to_celsius(x)", unit="*C")
        nickname, val, unit = ptx.apply_calib(107.79)

        assert unit == '*C'
        # 107.79 Ohm should be around 293 K (20 *C)
        assert round(val + 0.5) == 20

    # TODO:: Tests for ADRangeChannel
