# PyTest tests for calibration classes
#
# Indrek Synter 2019
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

from dlogger.calibration.temperature.rtd import PTX


class TestClass:
    def ptx(self, r0):
        # Multiplier to scale PT100 resistances to those of PT1000.
        f = r0 / 100

        # Have a zero-resistance slightly offset from 100 or 1000 Ohms.
        # Have A, B, C coefficients identical to those specified in the ITU-90 standard.
        ptx = PTX(r0 + 0.1863)

        # Fit the polynomial to maximum residual of 0.01 K.
        ptx.fit(0.01)
        assert len(ptx.fit_p) > 0
        assert max(ptx.fit_residuals) <= 0.01

        # Resistance at -200 *C should be ~19 Ohms (PT100) or ~190 Ohms (PT1000).
        r = ptx.resistance(73)
        assert r > 18.0 * f
        t = ptx.temperature(r)
        assert abs(t - 73) <= 0.01

        # Resistance at -40 *C should be ~84 Ohms (PT100) or ~840 Ohms (PT1000).
        r = ptx.resistance(253)
        assert r > 84.0 * f
        t = ptx.temperature(r)
        assert abs(t - 253) <= 0.01

        # Resistance at 0 *C should equal r0.
        r = ptx.resistance(273.15)
        assert r == ptx.r0
        t = ptx.temperature(r)
        assert abs(t - 273.15) <= 0.01

        # Resistance at 20 *C should be ~108 Ohms (PT100) or ~1080 Ohms (PT1000).
        r = ptx.resistance(293)
        assert r > 107.0 * f
        t = ptx.temperature(r)
        assert abs(t - 293) <= 0.01

        # Resistance at 600 *C should be ~314 Ohms (PT100) or ~3140 Ohms (PT1000).
        r = ptx.resistance(873)
        assert r > 313.0 * f
        t = ptx.temperature(r)
        assert abs(t - 873) <= 0.01

    def test_pt100(self):
        """Test if PT100 calibration works."""
        self.ptx(100)

    def test_pt1000(self):
        """Test if PT1000 calibration works."""
        self.ptx(1000)
