# Generic data logger device
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import logging

from dlogger.channel.generic_channel import Channel


class Device:
    """Generic base class for data logging devices."""

    CHANNEL_CLASS = Channel
    SUPPORTED_SOURCES = []
    DEVICE_INDEX = 0

    def __init__(self, class_name, nickname, simulate=True):
        """Initialize a device by giving it a name."""
        self.class_name = class_name
        self.nickname = nickname
        self.log = logging.getLogger("DLogger.{}".format(nickname))

        # Whether or not to simulate a real device.
        self.simulate = simulate

        # List of channels with a lookup dict.
        self.channels = []

        # Count devices.
        self.dev_id = Device.DEVICE_INDEX
        Device.DEVICE_INDEX += 1

    def __del__(self):
        """Deinitialize the device. Calls close() unless it's a simulated device."""
        self.log.debug("Closing {}..".format(self.nickname))
        if not self.simulate:
            self.close()

    def add_channel(self, name, source, formula="x", unit=""):
        """Add a measurement channel with a calibration formula and output unit."""
        # TODO:: Check against channel count
        c = self.CHANNEL_CLASS(name, source, formula, unit)
        self.channels.append(c)

    def num_channels(self):
        """Get the number of active channels."""
        return len(self.channels)

    def list_channels(self):
        """List channel names in use."""
        chlist = []
        for c in self.channels:
            if c.unit not in [None, ""]:
                chan = "{} / {}".format(c.nickname, c.unit)
            else:
                chan = c.nickname
            chlist.append(chan)
        return chlist

    def list_supported_sources(self):
        """List supported sources with short descriptions.

        Returns:
            List of pairs (source_name, description)
        """
        return self.SUPPORTED_SOURCES

    def text_supported_sources(self):
        """Multiline text describing the supported sources.

        Returns:
            Multiline string, a source with description on each line.
        """
        text = ""
        for c in self.SUPPORTED_SOURCES:
            text += "{} \t{}\n".format(c[0], c[1])
        return text

    def get_raw_measurement(self, source_type, source_index):
        """Get a raw measurement from the specified channel.

        Returns:
            Raw measurement could be one of the following types: integer, float or string.
        """
        return self.measure(source_type, source_index)

    def get_measurements(self):
        """Get measurements from all registered channels.

        Returns:
            List of pairs (channel_name, calibrated_value, unit)
        """
        result = []

        # Get measurements
        for c in self.channels:
            source = c.get_source()
            raw_value = self.get_raw_measurement(source[0], source[1])
            value = c.apply_calib(raw_value)

            result.append(value)
        return result

    def open(self, *args):
        """An abstract function to connect to the device. Must be implemented for each device."""
        self.log.error("{} must implement the open() function.".format(self.class_name))

    def measure(self, source_type, source_index):
        """An abstract function to measure a channel on the device. Must be implemented for each device."""
        self.log.error("{} must implement the measure() function.".format(self.class_name))

    def close(self):
        """An abstract function to disconnect the device. Must be implemented for each device."""
        self.log.error("{} must implement the close() function.".format(self.class_name))
