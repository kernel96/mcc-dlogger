# Clock device
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import time

from dlogger.device.generic_device import Device


class Clock(Device):
    """Generic clock device, to log date, time or timestamps."""

    SUPPORTED_SOURCES = [
            ("DATE", "Current date as a string, in local time"),
            ("EPOCH", "Linux timestamp, in seconds since epoch"),
            ("SECONDS", "Test timestamp, number of seconds since the test was started"),
            ]

    def __init__(self, name="clock"):
        """Initialize a clock."""
        Device.__init__(self, "CLOCK", name)
        # Timestamp of the first measurement taken.
        self.first_measurement_time = 0

    def open(self):
        pass

    def close(self):
        pass

    def measure(self, source_type, source_index):
        # Remember the time when the test started.
        if self.first_measurement_time == 0:
            self.first_measurement_time = time.time()

        raw_value = 0
        # Channel for current date?
        if source_type == "DATE":
            raw_value = time.strftime("\"%c\"")
        # Channel for current timestamp?
        elif source_type == "EPOCH":
            raw_value = time.time()
            # Channel for number of seconds since the start of the test?
        elif source_type == "SECONDS":
            raw_value = time.time() - self.first_measurement_time
        else:
            self.log.error("Unsupported source channel type '{}'.".format(source_type))
        return raw_value
