# Data logger device that needs an A/D range for each measurement.
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

from dlogger.device.generic_device import Device
from dlogger.channel.adrange_channel import ADRangeChannel


class ADRangeDevice(Device):
    """Base class for data logging devices that need an A/D range for each measurement."""

    CHANNEL_CLASS = ADRangeChannel

    SUPPORTED_RANGES = []

    def __init__(self, class_name, nickname, simulate=True):
        """Initialize a device by giving it a name."""
        Device.__init__(self, class_name, nickname, simulate)

    def __del__(self):
        """Deinitialize the device. Calls close() unless it's a simulated device."""
        Device.__del__(self)

    @staticmethod
    def sort_range(ad_range):
        """Make sure that the range is a pair (min, max)."""
        # Make sure that the range is given in the correct order.
        a, b = ad_range
        if a > b:
            return b, a
        else:
            return ad_range

    def get_nearest_range(self, ad_range):
        """Get the closest range from the list of supported ranges (min, max, id)."""
        nearest = None
        best_coverage = 0

        # Find the closest match from the ranges.
        # The match must be equal to or slightly larger than the requested range.
        # Larger ranges have less resolution, so prefer smaller ranges.
        for r in self.SUPPORTED_RANGES:
            if ad_range[0] >= r[0] and ad_range[1] <= r[1]:
                coverage = (ad_range[1] - ad_range[0]) / (r[1] - r[0])
                if coverage > best_coverage:
                    nearest = r
                    best_coverage = coverage
        self.log.debug("Nearest range for " + str(ad_range) + " is " + str(nearest))
        return nearest

    def add_channel(self, name, source, ad_range, formula="x", unit=""):
        """Add a measurement channel with an A/D range, a calibration formula and output unit."""
        # Make sure that the range is ordered correctly (min, max).
        ad_range = self.sort_range(ad_range)
        # Find the closest match to the requested range.
        range_match = self.get_nearest_range(ad_range)
        # Create a channel and add it.
        c = self.CHANNEL_CLASS(name, source, range_match, formula, unit)
        self.channels.append(c)
   
    def get_raw_measurement(self, source_type, source_index, range_desc):
        """Get a raw measurement from the specified channel.

        Returns:
            Raw measurement could be one of the following types: integer, float or string.
        """
        return self.measure(source_type, source_index, range_desc)
 
    def get_measurements(self):
        """Get measurements from all registered channels.
 
        Returns:
            List of pairs (channel_name, calibrated_value, unit)
        """
        result = []

        # Get measurements
        for c in self.channels:
            source = c.get_source()
            range_desc = c.get_ad_range()
            raw_value = self.get_raw_measurement(source[0], source[1], range_desc)
            value = c.apply_calib(raw_value)

            result.append(value)
        return result
