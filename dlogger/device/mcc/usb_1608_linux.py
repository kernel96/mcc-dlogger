# MCC USB-1608 data logger device, Linux port
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import random

from dlogger.device.adrange_device import ADRangeDevice


class MCC_1608(ADRangeDevice):
    """MCC USB-1608GX-2AO DAQ voltage logger with a USB DAQ interface."""

    SUPPORTED_SOURCES = [
            ("U0", "Raw voltage on CH0 (V)"),
            ("U1", "Raw voltage on CH1 (V)"),
            ("U2", "Raw voltage on CH2 (V)"),
            ("U3", "Raw voltage on CH3 (V)"),
            ("U4", "Raw voltage on CH4 (V)"),
            ("U5", "Raw voltage on CH5 (V)"),
            ("U6", "Raw voltage on CH6 (V)"),
            ("U7", "Raw voltage on CH7 (V)"),
            ("U8", "Raw voltage on CH8 (V)"),
            ("U9", "Raw voltage on CH9 (V)"),
            ("U10", "Raw voltage on CH10 (V)"),
            ("U11", "Raw voltage on CH11 (V)"),
            ("U12", "Raw voltage on CH12 (V)"),
            ("U13", "Raw voltage on CH13 (V)"),
            ("U14", "Raw voltage on CH14 (V)"),
            ("U15", "Raw voltage on CH15 (V)"),
            ]

    SUPPORTED_RANGES = [
            (-10, 10, 1), (-5, 5, 0), 
            (-2, 2, 14), (-1, 1, 4)
            ]

    def __init__(self, name="MCC_1608", simulate=True):
        """Initialize an MCC USB-1608 DAQ without connecting to it yet."""
        ADRangeDevice.__init__(self, "MCC_1608", name, simulate)

        self.log.error("Linux support for MCC USB-1608 is not implemented yet.")

        self.dev = None

    def open(self, serial=None):
        """Look for a USB device and connect to it."""
        if not self.simulate:
            pass
        else:
            self.log.info("Simulating a DAQ..")
            random.seed()

    def close(self):
        """Disconnect from the USB device (if any)."""
        pass

    def blink_led(self):
        """Blinks the LED four times."""
        pass

    def get_voltage(self, channel, ad_range):
        """Read voltage on a channel."""
        if not self.simulate:
            pass
        else:
            return ad_range[0] + (ad_range[1] - ad_range[0]) * random.random()
        return None

    def measure(self, source_type, source_index, range_desc):
        """Measure a channel by its type and index."""
        raw_value = 0
        if source_type == "U":
            raw_value = self.get_voltage(source_index, range_desc)
        else:
            self.log.error("Unsupported source channel type '{}'.".format(source_type))
        return raw_value
