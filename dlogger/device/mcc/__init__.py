import os
import sys

# Different backends on Windows and Linux
if os.name == "nt":
    import dlogger.device.mcc.usb_1608_win

    sys.modules["dlogger.device.mcc.usb_1608"] = sys.modules["dlogger.device.mcc.usb_1608_win"]
else:
    import dlogger.device.mcc.usb_1608_linux

    sys.modules["dlogger.device.mcc.usb_1608"] = sys.modules["dlogger.device.mcc.usb_1608_linux"]

