# Generic data storage class
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import logging


class Storage:
    """Generic storage base class."""

    def __init__(self, class_name, nickname):
        """Initialize the storage."""
        self.class_name = class_name
        self.nickname = nickname
        self.log = logging.getLogger("DLogger.{}".format(nickname))

        self.column_headers = []

    def __del__(self):
        """Deinitialize the storage."""
        self.close()

    def set_header(self, columns):
        """Set column headers.

        Args:
            columns (:obj:`list` of :obj:`str`): List of column headers.
        """
        self.column_headers = columns

    def open(self, *args):
        """An abstract function to open the storage. Must be implemented for each storage class."""
        self.log.error("{} must implement the open() function.".format(self.class_name))

    def write(self, row):
        """Store a row of measurements into the storage."""
        self.log.error("{} must implement the write() function.".format(self.class_name))

    def close(self):
        """An abstract function to close the storage. Must be implemented for each storage class."""
        self.log.error("{} must implement the close() function.".format(self.class_name))
