# Generic data logger channel
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import logging


class Channel:
    """Generic base class for measurement channel."""

    def __init__(self, nickname, source, formula="x", unit="", class_name="Channel"):
        """Initialize a measurement channel."""
        self.log = logging.getLogger("DLogger.{}({})".format(class_name, nickname))

        self.class_name = class_name
        self.nickname = nickname
        self.source = source
        self.formula = formula
        self.unit = unit

        self.global_scope = {
            "math": __import__("math")
        }

        # TODO:: Validate formula

    def get_source(self):
        """Get source type (string) and index (integer).

        Returns:
            Pair ("type", source_index)
        """
        # http://stackoverflow.com/questions/430079/how-to-split-strings-into-text-and-number
        head = self.source.rstrip("0123456789")
        tail = self.source[len(head):]
        if tail != "":
            tail = int(tail)
        else:
            tail = 0
        return head, tail

    def apply_calib(self, raw_value):
        """Apply calibration formula for the given raw value.

        Returns:
            Pair of corrected value and its unit.
        """
        # Only allow for the calibration of non-strings
        if not isinstance(raw_value, basestring):
            local_scope = {'x': raw_value}
            return self.nickname, eval(self.formula, self.global_scope, local_scope), self.unit
        else:
            return self.nickname, raw_value, self.unit
