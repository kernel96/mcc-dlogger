# Calibration functions for Resistance Thermometer Devices (RTD)
#
# Indrek Synter 2019
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

# References:
#  https://techoverflow.net/2016/01/02/accurate-calculation-of-pt100pt1000-temperature-from-resistance/

import math
import numpy as np
#import matplotlib.pyplot as plt


class PTX(object):
    """Calibration functions for a PT100 or PT1000 sensor."""

    # 0 *C in Kelvin
    K_0C = 273.15

    # Calibration coefficients, per ITU-90
    ITU90_A = 3.9083e-3
    ITU90_B = -5.775e-7
    ITU90_C = -4.183e-12

    # Limits to polynomial ranks
    POLY_MIN_RANK = 5
    POLY_MAX_RANK = 32

    def __init__(self, r0, a=None, b=None, c=None):
        # Resistance (Ohms) at 0 *C (273.15 K).
        # Should be around 100 Ohms for PT100 and 1000 Ohms for PT1000.
        self.r0 = r0

        # Calibration coefficients with defaults from ITU-90.
        self.a = a
        if not a:
            self.a = self.ITU90_A
        self.b = b
        if not b:
            self.b = self.ITU90_B
        self.c = c
        if not c:
            self.c = self.ITU90_C

        self.fit_p = None
        self.fit_residuals = None
        self.fit_rank = None
        self.fit_singular_values = None
        self.fit_rcond = None

    def fit(self, max_residual=0.1):
        """Fits a polynomial for the negative temperature range of the resistance curve.

        Returns:
            True if the max_residual criteria was met, otherwise returns False."""
        # From -273 *C to 0 *C with a step of 1 *C.
        y = np.arange(0, PTX.K_0C, 1)
        x = self.resistance(y)

        # TODO:: Find a function instead of fitting polynomials.

        # Polynomials from 5th to 32nd rank.
        for deg in range(PTX.POLY_MIN_RANK, PTX.POLY_MAX_RANK):
            p, residuals, rank, singular_values, rcond = np.polyfit(x, y, deg, full=True)

            self.fit_p = p
            self.fit_residuals = residuals
            self.fit_rank = rank
            self.fit_singular_values = singular_values
            self.fit_rcond = rcond

            if max(residuals) < max_residual:
                return True
        return False

#    def plot(self):
#        """Plot polynomial error, for debugging purposes only."""
#        y = np.arange(0, PTX.K_0C, 1)
#        x = self.resistance(y)
#
#        p = np.poly1d(self.fit_p)
#        plt.plot(y - PTX.K_0C, y - p(x), '-')
#        plt.legend(["data"])
#        plt.show()

    def resistance(self, t):
        """Convert temperature (K) into resistance (Ohms).

        Returns:
            Resistance in Ohms."""
        # Convert from K to *C.
        x = t - PTX.K_0C
        # Note that the last component is zeroed when temperature is above 0 *C.
        return self.r0 * (1 + self.a * x + self.b * x**2 + (x < 0) * self.c * (x - 100) * x**3)

    def temperature(self, r):
        """Convert resistance (Ohms) into temperature (K).

        Returns:
            Temperature in Kelvin."""
        assert self.fit_p is not None, "The polynomial must be initialized first, using the fit() function."

        # Temperature at 0 *C or above?
        if r >= self.r0:
            a, b, r0 = self.a, self.b, self.r0
            return (math.sqrt(r0 * a**2 + 4 * b * r - 4 * b * r0) / math.sqrt(r0) - a) / (2 * b) + PTX.K_0C
        else:
            return np.poly1d(self.fit_p)(r)
