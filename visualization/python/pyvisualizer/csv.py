#!/usr/bin/python
#
# Python Visualizer CSV parser.
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import logging


class CSV:
    DELIMITERS = ";\t,"
    UNIT_SEPARATOR = "/"

    def __init__(self):
        """Initialize the CSV parser."""
        self.log = logging.getLogger("PV.CSV")

        self.filepath = None

        self.header = []
        self.data = []
        self.units = []

        self.x_axis = []

    def find_delimiter(self, line):
        for d in self.DELIMITERS:
            if line.count(d) >= 1:
                return d
        self.log.warning("Failed to find a good delimiter. "
                         "Expecting at least 2 columns with any of the following delimiters: ;\\t,")
        return self.DELIMITERS[0]

    @staticmethod
    def convert(x):
        try:
            return float(x)
        except ValueError:
            return x

    def load(self, filepath):
        """Load a CSV file."""
        self.filepath = filepath

        self.header = []
        self.data = []

        # Extract header and measurements.
        with open(filepath, "rt") as fi:
            delim = None
            for line in fi:
                # Find a good delimiter, based on the header column.
                if delim is None:
                    delim = self.find_delimiter(line)

                tok = line.split(delim)
                tok = [x.strip(" \t\"\r\n") for x in tok]
                if not self.header:
                    self.header = tok
                else:
                    tok = [self.convert(x) for x in tok]
                    self.data.append(tok)

        self.detect_units()

    def detect_units(self):
        """Try to detect units from column names."""
        self.units = ['']*len(self.header)
        for i in range(0, len(self.header)):
            unit = self.extract_unit(self.header[i])
            self.units[i] = unit

    def extract_unit(self, label):
        """Extract unit name from column header (the standard is Name / Unit)."""
        try:
            unit = ""
            unit = label.split(self.UNIT_SEPARATOR, 1)[1].strip()
        except Exception as e:
            pass
        finally:
            return unit

    def get_num_rows(self):
        """Get the number of rows."""
        return len(self.data)

    def get_num_cols(self):
        """Get the number of columns."""
        return len(self.header)
    
    def get_header(self):
        """Get header columns."""
        return self.header

    def get_units(self):
        """Get header column units."""
        return self.units
    
    def get_data(self):
        """Get data."""
        return self.data

