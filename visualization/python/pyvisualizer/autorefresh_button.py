#!/usr/bin/python
#
# Python Visualizer auto-refresh button.
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

from PySide2 import QtGui, QtCore, QtWidgets

from .config import Config


class AutoRefreshButton(QtWidgets.QToolButton):
    # Auto-refresh settings were updated.
    sig_autorefresh_set = QtCore.Signal(bool, float)

    def __init__(self, parent):
        """Initialize the auto-refresh button."""
        QtWidgets.QToolButton.__init__(self)
        self.setParent(parent)

        self.config = Config()

        self.setIcon(QtGui.QIcon("pyvisualizer/icons/arrow-circle-double.png"))
        self.setAutoRaise(True)
        self.setCheckable(True)
        self.update_tooltip()

        self.menu = None
        self.create_context_menu()

        self.toggled.connect(self.was_toggled)

    def create_menu_okbutton(self, parent):
        """Create a context menu entry for an OK button."""
        ok_button = QtWidgets.QPushButton("Ok")
        ok_button.clicked.connect(self.do_update)

        widget_action = QtWidgets.QWidgetAction(parent)
        widget_action.setDefaultWidget(ok_button)
        return widget_action

    def create_menu_period_edit(self, parent):
        """Create a context menu for editing the autorefresh period."""
        container = QtWidgets.QWidget(parent)
        layout = QtWidgets.QHBoxLayout()
        layout.setContentsMargins(2, 2, 2, 2)

        period_label = QtWidgets.QLabel(container)
        period_label.setText("Period:")

        period_edit = QtWidgets.QDoubleSpinBox(container)
        period_edit.setValue(self.config.autorefresh_period)
        period_edit.setDecimals(1)
        period_edit.setMaximumWidth(60)
        period_edit.valueChanged.connect(self.period_edited)

        layout.addWidget(period_label)
        layout.addWidget(period_edit)

        container.setLayout(layout)

        widget_action = QtWidgets.QWidgetAction(parent)
        widget_action.setDefaultWidget(container)
        return widget_action

    def create_context_menu(self):
        """Custom context menu on right-click."""
        # Thanks to http://www.ffuts.org/blog/right-click-context-menus-with-qt/
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.show_context_menu)

        self.menu = QtWidgets.QMenu(self)
        period_edit = self.create_menu_period_edit(self.menu)
        ok_button = self.create_menu_okbutton(self.menu)
        self.menu.addAction(period_edit)
        self.menu.addAction(ok_button)

    def show_context_menu(self, pos):
        """Show context menu that allows for editing the refresh period."""
        # Make sure that the menu is created at the click
        g_pos = self.mapToGlobal(pos)
        # Show it at the right position
        self.menu.exec_(g_pos)

    def update_tooltip(self):
        """Update tooltip, based on button state."""
        if self.isChecked():
            self.setToolTip("Auto-refresh every {} s, left-click to disable, right-click to edit."
                            .format(self.config.autorefresh_period))
        else:
            self.setToolTip("Auto-refresh disabled, left-click to enable.")

    def period_edited(self, new_period):
        """Callback for changes to the auto-refresh period."""
        self.config.autorefresh_period = new_period
    
    def was_toggled(self, _):
        """The button was toggled. Update the tooltip."""
        self.config.autorefresh_enabled = self.isChecked()
        self.update_tooltip()
        self.sig_autorefresh_set.emit(self.isChecked(), self.config.autorefresh_period)

    def do_update(self):
        """Close the context menu, update the tooltip and propagate the signals."""
        self.menu.hide()
        self.update_tooltip()
        self.sig_autorefresh_set.emit(self.isChecked(), self.config.autorefresh_period)
