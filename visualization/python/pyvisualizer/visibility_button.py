#!/usr/bin/python
#
# Python Visualizer button for series visibility.
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

from PySide2 import QtGui, QtCore, QtWidgets


class VisibilityButton(QtWidgets.QToolButton):
    sig_state_changed = QtCore.Signal(int)

    ICONS = ["pyvisualizer/icons/bullet_white.png",
             "pyvisualizer/icons/bullet_green.png"]

    STATE_HIDDEN = 0
    STATE_SHOWN = 1

    def __init__(self, parent):
        """Initialize the show-hide button."""
        QtWidgets.QToolButton.__init__(self)
        self.setParent(parent)

        self.state = self.STATE_SHOWN

        self.setIcon(self.get_icon())
        self.setAutoRaise(True)
        self.setToolTip("Show / hide series")

        # Not using a checkable button because don't want it to be sunken.
        # Could work around this issue with stylesheets as well, tho.
        self.clicked.connect(self.toggle_state)

    def get_icon(self):
        """Get the icon that reflects the state."""
        return QtGui.QIcon(self.ICONS[self.state])
    
    def toggle_state(self):
        """Toggle the state."""
        if self.state == self.STATE_SHOWN:
            self.state = self.STATE_HIDDEN
        else:
            self.state = self.STATE_SHOWN
        self.setIcon(self.get_icon())
        self.sig_state_changed.emit(self.state)

    def is_shown(self):
        """Check if the series is supposed to be shown."""
        return self.state == self.STATE_SHOWN
