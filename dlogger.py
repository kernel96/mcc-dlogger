#!/usr/bin/python
#
# MCC USB-TEMP data logger
#
# Indrek Synter 2017
# Space Technology Department, Tartu Observatory
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import logging
import logging.handlers
import argparse
import os
import sys
import time

from dlogger.config import Config


class LinuxLogColorFormatter(logging.Formatter):
    """Python logging color formatter.
    http://stackoverflow.com/questions/384076/how-can-i-color-python-logging-output
    """
    FORMAT = ("[%(levelname)-18s][%(name)-20s]  "
              "%(message)s "
              "($BOLD%(filename)s$RESET:%(lineno)d)")
    BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(8)

    COLORS = {
        'WARNING': YELLOW,
        'INFO': WHITE,
        'DEBUG': BLUE,
        'CRITICAL': YELLOW,
        'ERROR': RED,
        'RED': RED,
        'GREEN': GREEN,
        'YELLOW': YELLOW,
        'BLUE': BLUE,
        'MAGENTA': MAGENTA,
        'CYAN': CYAN,
        'WHITE': WHITE,
    }

    RESET_SEQ = "\033[0m"
    COLOR_SEQ = "\033[1;%dm"
    BOLD_SEQ = "\033[1m"

    def __init__(self, use_color=True):
        """Initialize the formatter."""
        self.use_color = use_color
        if use_color:
            msg = self.FORMAT.replace("$RESET", self.RESET_SEQ).replace("$BOLD", self.BOLD_SEQ)
        else:
            msg = self.FORMAT.replace("$RESET", "").replace("$BOLD", "")

        logging.Formatter.__init__(self, msg)

    def format(self, record):
        """Format a log record."""
        levelname = record.levelname
        if self.use_color and levelname in self.COLORS:
            fg_color = 30 + self.COLORS[levelname]
            levelname_color = self.COLOR_SEQ % fg_color + levelname + self.RESET_SEQ
            record.levelname = levelname_color
        return logging.Formatter.format(self, record)


class LogInfoFilter(logging.Filter):
    """A logging filter that discards warnings and errors."""
    def filter(self, record):
        """False on warning or error records."""
        return record.levelno in (logging.DEBUG, logging.INFO)


def init_logging(opts, logfile):
    """Initialize logging, based on verbosity level."""
    # Configure logging
    if opts.verbose == 0:
        log_level = logging.WARNING
    elif opts.verbose == 1:
        log_level = logging.INFO
    elif opts.verbose == 2:
        log_level = logging.DEBUG
    else:
        log_level = logging.NOTSET

    # logging consists of hierarchical filters.
    # ROOT (ERROR, WARNING, INFO, DEBUG)
    #   Stdout (log_level)
    #   Stderr (ERROR, WARNING)
    #   Logfile (ERROR, WARNING, INFO, DEBUG)

    log = logging.getLogger("DLogger")
    log.setLevel(logging.DEBUG)

    # Create log formatters.
    log_formatter = logging.Formatter('%(asctime)s: %(levelname)s: %(name)s: %(message)s')
    if os.name != 'nt':
        stdout_formatter = LinuxLogColorFormatter()
    else:
        # For colored output in Windows, refer to sorin's answer in
        # http://stackoverflow.com/questions/384076/how-can-i-color-python-logging-output
        stdout_formatter = logging.Formatter('%(levelname)s: %(name)s: %(message)s')

    # Info, debug messages to stdout (if we have enough verbosity).
    stdout_handler = logging.StreamHandler(sys.stdout)
    stdout_handler.setLevel(log_level)
    stdout_handler.setFormatter(stdout_formatter)
    stdout_handler.addFilter(LogInfoFilter())

    # Warnings and errors to stderr.
    stderr_handler = logging.StreamHandler(sys.stderr)
    stderr_handler.setLevel(logging.WARNING)
    stderr_handler.setFormatter(stdout_formatter)

    # Rotating log up to 1 MB.
    log_handler = logging.handlers.RotatingFileHandler(logfile, maxBytes=1024*1024, backupCount=3)
    log_handler.setLevel(logging.DEBUG)
    log_handler.setFormatter(log_formatter)

    # Add the stream handlers.
    log.addHandler(log_handler)
    log.addHandler(stderr_handler)
    log.addHandler(stdout_handler)

    log.info('Data logger started..')

    return log


def main():
    """Application main loop, which stops on Ctrl+C."""
    p = argparse.ArgumentParser()
    p.add_argument("-o", "--outfile", dest="outfile", default="")
    p.add_argument("-c", "--cfg", dest="cfgfile", default="")
    p.add_argument("-p", "--period", dest="period", default=0.0)
    p.add_argument("-s", "--simulate", dest="simulate", action="store_true", default=False)
    p.add_argument("-l", "--log", dest="logfile", default="dlogger.log")
    p.add_argument("-v", "--verbose", dest="verbose", action="count", default=0,
                   help="Increase verbosity (specify multiple times for more)")

    arguments = p.parse_args()

    log = None
    try:
        log = init_logging(arguments, arguments.logfile)

        # Load config.
        cfg = Config(arguments.cfgfile)
        # Update a few parameters that can be overridden from the command-line.
        if arguments.outfile != "":
            # Redirect the first CSV endpoint to the specified file.
            storages = cfg.get_storage_endpoints("CSV")
            if len(storages) == 0:
                s = cfg.add_storage("CSV")
            else:
                s = storages[0]
            s.open(arguments.outfile)
        if arguments.period > 0.0:
            cfg.set_measurement_period(arguments.period)

        # Measurement devices.
        devices = cfg.get_devices()
        # CSV, database, server, etc.
        storages = cfg.get_storage_endpoints()

        # Start measurements.
        print("Looping until Ctrl+C is pressed..")
        try:
            while True:
                # Allocate for a row.
                data_row = [None]*cfg.num_channels()

                # Measure all devices sequentially.
                i = 0
                for d in devices:
                    measurements = d.get_measurements()
                    for m in measurements:
                        # Only store the calibrated values.
                        data_row[i] = m[1]
                        i += 1

                # Store the row.
                for s in storages:
                    s.write(data_row)

                # Wait for the next period
                time.sleep(cfg.measurement_period)
        except KeyboardInterrupt:
            pass

        # Close storage endpoints.
        log.info("Deinitializing storage endpoints..")
        for s in storages:
            s.close()

        # Close devices.
        log.info("Deinitializing devices..")
        for d in devices:
            d.close()
    except Exception as e:
        if log is not None:
            log.exception("Unhandled exception")
        else:
            print "Failed to initialize error logging: " + str(e)


if __name__ == '__main__':
    main()
