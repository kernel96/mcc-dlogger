# README #

## Introduction ##
Although InstaCal supports resistance measurements, it does not support logging of the measurements. Whereas TracerDAQ supports logging of measurements, it does not support resistances. Because of this, custom calibration formulas are not supported.

As a work-around, the MCC USB-TEMP DAQ USB protocol was reverse-engineered and a Python script with HIDAPI was written for logging resistance measurements. Let's call it DLogger.

## How to install ##
### Windows ###
1. Download and install [Python 2.7](https://www.python.org/downloads/).
1. Download and install [Microsoft Visual C++ 9.0 for Cython](http://aka.ms/vcpython27).
1. Browse to the Python27/Scripts folder.
1. Open command terminal from there, by holding Shift and right-clicking inside the folder. Then select "Open command window here".
1. Install cython-hidapi by typing the following into the terminal:
        ```
        pip.exe install hidapi
        ```
    * If it fails with "unable to find vcvarsall.bat", then the installation of vcpython27 was not successful.
1. Install mcculw:
        ```
        pip.exe install mcculw
        ```
1. Update your installation of [MCC DAQ CD](https://www.mccdaq.com/Software-Downloads.aspx). In particular, it is the "Universal Library" that needs to be up to date in order to work with mcculw.
1. Git clone mcc-dlogger.

### Ubuntu / Debian ###
1. Install the following packages:
    ```
    python2.7 python-pip cython
    ```
1. Install the following python modules with pip:
    ```
    hidapi
	pyusb
    ```
1. Git clone and install [cython-hidapi](https://github.com/gbishop/cython-hidapi).

## How to configure ##
DLogger accepts python files for configuration.
```
#!python
# Create a CSV storage endpoint. that stores measurements in "test.csv".
csv = add_storage("CSV")
# Set CSV delimiter to comma, for compatibility with Microsoft Excel.
csv.set_delimiter(",")
# Store CSV rows in "test.csv"; flush the file with 1 s interval (default).
csv.open("test.csv", 1)

# Create a clock device for date, timestamp and time since the start of test.
clock = add_device("CLOCK")
# For clock, an open() call is not really needed.
clock.open()
# Add a column with string date, for example: "Mon Apr 25 11:19:15 2016".
clock.add_channel("Date", "DATE")
# Add a column with unix timestamp.
clock.add_channel("Epoch", "EPOCH")
# Add a column with seconds since the start of test.
clock.add_channel("Time", "SECONDS")

# Add an MCC USB-TEMP device with serial number 1811A3
temp = add_device("MCC_USB_TEMP")
temp.open("1811A3")
# Add a column "R0" with the resistance value of channel 0.
temp.add_channel("R0", "R0", "x", "Ohm")
# Add a column "T0" with the temperature value of channel 0 (formula M0 = Ax^2 + Bx + C).
temp.add_channel("T0", "R0", "-4.02885E-04*x**2 + 6.91783E-01*x - 1.36453E+02", "*C")
# Add a column "R1" with the resistance value of channel 1, in kilo-ohms, for example.
temp.add_channel("R1", "R1", "x/1000.0", "kOhm")
# Add a column "T1" with the temperature value of channel 1 (formula 1/T = A + B*ln(x) + C*ln(x)^3).
temp.add_channel("T1", "R1", "1/(1.811648E-02 - 3.216568E-03*math.log(x) + 1.871540E-05*math.log(x)**3)", "*C")
# Add a column "T2" with the temperature value of channel 2 (calibration performed in hardware, based on InstaCal configuration).
temp.add_channel("T2", "T2", "x", "*C")
# Add a PT100 column with specific R0, A, B, C values, a maximum residual of 0.01 K and convert result to celsius.
temp.add_ptx_channel("T3", "R3", 100.1863, 3.8766e-3, -4.3986e-7, -2.0332e-10, 0.01, "kelvin_to_celsius(x)", "*C")
# Add columns for DAQ internal temperatures.
temp.add_channel("DAQint0", "CJC0", "x", "*C")
temp.add_channel("DAQint1", "CJC1", "x", "*C")

# Add an MCC USB-1608GX-2AO with any serial number (when just a single device is connected).
volt = add_device("MCC_1608")
volt.open()
# Measure "Voltage" on the first channel (U0), within range -10 .. +10 V (ULRange.BIP10VOLTS which corresponds to the value 1).
volt.add_channel("Voltage", "U0", (-10, 10, 1), "x", "V")

# Measure all the channels with a period of 100 ms.
set_measurement_period(0.1)
```

## How to run ##
```
python dlogger.py -c your_test_config.cfg
```

## To do ##
* Proper documentation for MCC USB-1608 channels with A/D ranges.
* Linux support for MCC USB-1608.
* Produce a PyInstaller stand-alone for Windows.
* Implement a web server for streaming JSON or CSV.
* Make figures more configurable.
* Add support for saving the chart as SVG, PDF or PNG.
* Make JavaScript visualizer more configurable.
* Add GNUPlot visualizer.

### Who do I talk to? ###
Repo owner
